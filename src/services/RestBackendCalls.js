import axios from "axios";

export default {
  doPostRequest(location, payload, options) {
    console.debug("doPostRequest was called");

    return axios
      .post(location, {
        data: payload,
        ...options,
      })
      .then((resp) => {
        console.log("clientCall ->", resp);
        return resp;
      })
      .catch((error) => {
        console.error("clientCall", error);
        return -1;
      });
  },
  doPutRequest(location, payload, options) {
    console.debug("doPutRequest was called");
    return axios
      .put(location, {
        data: payload,
        ...options,
      })
      .then((resp) => {
        console.log("doPutRequest(), clientCall ->", resp);
        return resp;
      })
      .catch((error) => {
        console.error("doPutRequest(), clientCall", error);
        return -1;
      });
  },
  doGetRequest(location, options) {
    console.debug("doGetRequest was called");
    return axios
      .get(location, {
        ...options,
      })
      .then((resp) => {
        console.log("doGetRequest(), clientCall ->", resp);
        return resp;
      })
      .catch((error) => {
        console.error("doGetRequest(), clientCall", error);
        return -1;
      });
  },
  doPatchRequest(location, payload, options) {
    console.debug("doPatchRequest was called");
    return axios
      .patch(location, {
        url: location,
        data: payload,
        ...options,
      })
      .then((resp) => {
        console.log("doPatchRequest() clientCall ->", resp);
        return resp;
      })
      .catch((error) => {
        console.error("doPatchRequest(), clientCall", error);
        return -1;
      });
  },
};
