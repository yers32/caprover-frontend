import Vue from "vue";
import Vuex from "vuex";
import defaultModule from "./modules/defaultModule";


Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    dmodule: defaultModule,
  },
});
